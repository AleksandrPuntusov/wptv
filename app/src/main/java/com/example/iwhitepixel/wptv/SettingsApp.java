package com.example.iwhitepixel.wptv;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SettingsApp {

    private static final String TAG = SettingsApp.class.getSimpleName();

    /**
     * Instance of SharedPreferences object
     */
    private SharedPreferences sPref;
    /**
     * Editor of SharedPreferences object
     */
    private SharedPreferences.Editor editor;

    private static SettingsApp ourInstance = new SettingsApp();

    /**
     * get instance settingsApp
     *
     * @return
     */
    public static SettingsApp getInstance() {
        return ourInstance;
    }


    /**
     * Construct the instance of the object
     */
    public SettingsApp() {
        sPref = PreferenceManager.getDefaultSharedPreferences(App.getContext());
        editor = sPref.edit();
    }


    // Keys for opening settings from xml file
    private static final String indexVideo = "indexVideo";
    private static final String currentPositions = "currentPositions";
    private static final String currentTimes = "currentTimes";

//    ===================================================================


    public int getIndexVideo() {
        return sPref.getInt(indexVideo, 0);
    }


    public void setIndexVideo(int ip) {
        editor.putInt(indexVideo, ip).commit();
    }
//    ===================================================================


    public float getCurrentPositions() {
        return sPref.getFloat(currentPositions, 0);
    }


    public void setCurrentPositions(float ip) {
        editor.putFloat(currentPositions, ip).commit();
    }
//    ===================================================================


    public long getCurrentTime() {
        return sPref.getLong(currentTimes, 0);
    }


    public void setCurrentTime(long ip) {
        editor.putLong(currentTimes, ip).commit();
    }

//    ===================================================================

}
