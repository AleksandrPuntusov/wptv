package com.example.iwhitepixel.wptv;

import android.Manifest;
import android.app.Activity;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

import org.videolan.libvlc.IVLCVout;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import wseemann.media.FFmpegMediaMetadataRetriever;

import static org.videolan.libvlc.MediaPlayer.Event.ESAdded;
import static org.videolan.libvlc.MediaPlayer.Event.ESDeleted;
import static org.videolan.libvlc.MediaPlayer.Event.EncounteredError;
import static org.videolan.libvlc.MediaPlayer.Event.EndReached;
import static org.videolan.libvlc.MediaPlayer.Event.Opening;
import static org.videolan.libvlc.MediaPlayer.Event.PausableChanged;
import static org.videolan.libvlc.MediaPlayer.Event.Paused;
import static org.videolan.libvlc.MediaPlayer.Event.Playing;
import static org.videolan.libvlc.MediaPlayer.Event.PositionChanged;
import static org.videolan.libvlc.MediaPlayer.Event.SeekableChanged;
import static org.videolan.libvlc.MediaPlayer.Event.Stopped;
import static org.videolan.libvlc.MediaPlayer.Event.TimeChanged;
import static org.videolan.libvlc.MediaPlayer.Event.Vout;

public class MainActivity extends AppCompatActivity {

    private int mVideoWidth;
    private int mVideoHeight;

    static final int MY_PERMISSION_REQUEST_EXTERNAL_STORAGE = 0;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.textView)
    TextView textView;
    @BindView(R.id.seekBar)
    SeekBar seekBar;
    @BindView(R.id.timerTextView)
    TextView timerTextView;
    @BindView(R.id.timeDurationTextView)
    TextView timeDurationTextView;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.logoImageView)
    ImageView logoImageView;
    @BindView(R.id.videoView)
    SurfaceView videoView;
    @BindView(R.id.playButton)
    ImageButton playButton;
    @BindView(R.id.nextButton)
    ImageButton nextButton;
    @BindView(R.id.prevButton)
    ImageButton prevButton;
    @BindView(R.id.main_layout)
    ViewGroup main_layout;

    private Thread thread;
    private boolean foundFile = false;
    private boolean hidenControl;
    private final static String EXTENDED_INFO_TAG = "#EXTM3U";
    private final static String RECORD_TAG = "^[#][E|e][X|x][T|t][I|i][N|n][F|f].*";
    private String m3uFilePath;
    private String videoPath;
    private String videoFilePath;
    private boolean isSeek;
    private ArrayList<String> videoArrayList = new ArrayList<>();
    private int indexVideo;
    private Handler handlerControl = new Handler();
    private Handler handlerConnectDisk = new Handler();

    private MediaPlayer mediaPlayer;
    private USB_BroadcastReceiver usb_broadcastReceiver;

    private Runnable doBackgroundThreadProcessing3 = new Runnable() {
        public void run() {
            videoPath = new File(m3uFilePath).getParent();
            videoArrayList.clear();
            pasreM3UFile();
            indexVideo = 0;
            if (videoArrayList.size() > 0) {
                isSeek = false;
                prepereIndex(indexVideo);
                runOnUiThread(() -> {
                    //Toast.makeText(MainActivity.this,m3uFilePath,Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                    textView.setVisibility(View.GONE);
                });
            }
        }
    };
    private Runnable handlerConnectDiskProcessing = new Runnable() {
        public void run() {
            if (new File(videoFilePath).exists()) {
                if (mediaPlayer.isPlaying()) {
                    return;
                }
                goToVideo();
            } else {
                startConnectDisk();
            }
        }
    };

    private Runnable runControl = () -> doHideControl(true);

    private Runnable doBackgroundThreadProcessing = () -> findFile();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        usb_broadcastReceiver = new USB_BroadcastReceiver();
        IntentFilter filterUsb = new IntentFilter();
        filterUsb.addAction(Intent.ACTION_BATTERY_CHANGED);
        filterUsb.addAction(Intent.ACTION_UMS_CONNECTED);
        filterUsb.addAction(Intent.ACTION_UMS_DISCONNECTED);
        registerReceiver(usb_broadcastReceiver, filterUsb);


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        progressBar.getIndeterminateDrawable().setColorFilter(0xFFFFFFFF, android.graphics.PorterDuff.Mode.MULTIPLY);
        playButton.setFocusable(true);
        nextButton.setFocusable(false);
        nextButton.setAlpha(0.3f);
        prevButton.setFocusable(false);
        prevButton.setAlpha(0.3f);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mediaPlayer.setTime(seekBar.getProgress());
            }
        });
    }

    private void hideSystemUI() {
        main_layout.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            textView.setText("Set allow access to storage in settings!");
            Snackbar.make(main_layout, "Extarnal storage access is required!",
                    Snackbar.LENGTH_SHORT).setAction("OK", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                            Manifest.permission.READ_EXTERNAL_STORAGE
                    }, MY_PERMISSION_REQUEST_EXTERNAL_STORAGE);
                }
            }).show();
        } else {
            Snackbar.make(main_layout, "Permission is not available. Requesting Extarnal storage  permission.",
                    Snackbar.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE
            }, MY_PERMISSION_REQUEST_EXTERNAL_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != MY_PERMISSION_REQUEST_EXTERNAL_STORAGE) {
            textView.setText("Set allow access to storage in settings!");
            return;
        }

        if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Snackbar.make(main_layout, "Extarnal storage  permission was granted.", Snackbar.LENGTH_SHORT).show();
            Intent intent = getIntent();
            Uri data = intent.getData();
            if (data != null) {
                m3uFilePath = data.getPath();
                thread = new Thread(null, doBackgroundThreadProcessing3,
                        "Background");
                thread.start();
            } else {
                new Handler().postDelayed(() -> {
                    thread = new Thread(null, doBackgroundThreadProcessing,
                            "Background");
                    thread.start();
                }, 6000);
            }
        } else {
            textView.setText("Set allow access to storage in settings!");
        }
    }

    private void findFile() {
        String sdpath;
        if (new File("/storage/").exists() &&
                !foundFile) {
            sdpath = "/storage/";
            findM3UFile(new File(sdpath));
        }
        if (new File("/sdcard/").exists() &&
                !foundFile) {
            sdpath = "/sdcard/";
            findM3UFile(new File(sdpath));
        }
        if (Environment.getExternalStorageDirectory().exists() && !foundFile) {
            findM3UFile(Environment.getExternalStorageDirectory());
        }
//        foundFiles();
    }

    private void foundFiles() {
        if (foundFile) {
            isSeek = true;
            videoArrayList.clear();
            pasreM3UFile();
            indexVideo = SettingsApp.getInstance().getIndexVideo();
            if (videoArrayList.size() > 0) {
                prepereIndex(indexVideo);
            }
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    textView.setText("Playlist file not found!");
                }
            });
        }
    }

    private void prepereIndex(int i) {
        if (i == videoArrayList.size()) {
            i = 0;
        } else if (i == -1) {
            i = videoArrayList.size() - 1;
        }
        for (int index = i; index < videoArrayList.size(); index++) {
            videoFilePath = videoArrayList.get(index);
            if (new File(videoFilePath).exists()) {
                indexVideo = index;
                SettingsApp.getInstance().setIndexVideo(indexVideo);
                runOnUiThread(() -> goToVideo());
                return;
            }
        }
    }

    private void findM3UFile(File directory) {
        final File[] files = directory.listFiles();
        if (files != null) {
            for (final File file : files) {
                String name = file.getName();
                Log.d("FILE_TAG", file.getAbsolutePath());
                Log.d("FILE_NAME", name);
                if (file != null) {
                    if (file.isDirectory()) {
                        findM3UFile(file);
                    } else if (file.getName().equalsIgnoreCase("НАЖМИ ЗДЕСЬ.m3u")) {
                        m3uFilePath = file.getAbsolutePath();
                        videoPath = file.getParent();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                // Toast.makeText(MainActivity.this,m3uFilePath,Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                                textView.setVisibility(View.GONE);
                            }
                        });
                        foundFile = true;
                        foundFiles();
                        return;
                    }
                }
            }
        }
    }

    private void pasreM3UFile() {
        File f = new File(m3uFilePath);
        try {
            String line = null;
            BufferedReader reader = null;
            reader = new BufferedReader(new InputStreamReader(new
                    FileInputStream(m3uFilePath)));
            while ((line = reader.readLine()) != null) {
                if (!(line.equalsIgnoreCase(EXTENDED_INFO_TAG) || line.trim().equals(""))) {
                    if (line.matches(RECORD_TAG)) {

                    } else {
                        String pathToFile = videoPath + "/" + line.trim();
                        if (new File(pathToFile).exists()) {
                            videoArrayList.add(pathToFile);
                        }
                    }
                }
            }
            Log.d("FILES_TAG", "Count files = " + videoArrayList.size());
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("FILES_TAG", "Count files = ERRORS" + e.getMessage());
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                doHideControl(false);
            }
        });
    }

    private void goToVideo() {
        String time = "0";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                retriever.setDataSource(MainActivity.this, Uri.parse(videoFilePath));
                time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            } catch (Exception e) {

            }
        } else {
            FFmpegMediaMetadataRetriever retriever = new FFmpegMediaMetadataRetriever();
            try {
                retriever.setDataSource(videoFilePath);
                time = retriever.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_DURATION);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        int duration = Integer.parseInt(time);
        timeDurationTextView.setText(getDuration(duration));
        seekBar.setMax(duration);
        ArrayList<String> opt = new ArrayList<String>();
        opt.add("--audio-time-stretch");
        opt.add("--aout=opensles");
        LibVLC lib = new LibVLC(opt);
        if (mediaPlayer == null) {
            mediaPlayer = new MediaPlayer(lib);
        } else {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = new MediaPlayer(lib);
        }
        mediaPlayer.setEventListener(new MediaPlayer.EventListener() {
            @Override
            public void onEvent(MediaPlayer.Event event) {
                if (event.type != PositionChanged && event.type != TimeChanged) {
//                    Toast.makeText(MainActivity.this, event.type + "", Toast.LENGTH_SHORT).show();
                }
                switch (event.type) {
                    case TimeChanged:
                        Log.d("Loging_plaer", "TimeChanged 267" + mediaPlayer.getTime());
                        break;
                    case EndReached:
                        Log.d("Loging_plaer", "EndReached 265" + mediaPlayer.getTime());
                        break;
                    case Stopped:
                        Log.d("Loging_plaer", "Stopped 262  " + mediaPlayer.getTime());
//                    Toast.makeText(MainActivity.this, "Stopped 262 __ " + mediaPlayer.getTime(), Toast.LENGTH_SHORT).show();
                        if (!new File(videoFilePath).exists()) {
                            startConnectDisk();
                        } else {
                            indexVideo++;
                            prepereIndex(indexVideo);
                        }
                        break;
                    case Opening:
                        Log.d("Loging_plaer", "Opening 258" + mediaPlayer.getTime());
                        handlerConnectDisk.removeCallbacks(handlerConnectDiskProcessing);
                        doHideControl(true);
                        String parent = new File(videoFilePath).getParent() + "/logotip.png";
                        Glide.with(MainActivity.this)
                                .load(parent)
                                .into(logoImageView);
                        Log.d("Loging_plaer", "file   " + parent);
                        break;
                    case Playing:
                        Log.d("Loging_plaer", "Playing 260" + mediaPlayer.getTime());
                        break;
                    case Paused:
//                        Log.d("Loging_plaer", "Paused 261" + mediaPlayer.getTime());
                        break;
                    case EncounteredError:
                        Log.d("Loging_plaer", "EncounteredError" + mediaPlayer.getTime());
                        startConnectDisk();
                        break;
                    case PositionChanged:
                        Log.d("Loging_plaer", "PositionChanged" + mediaPlayer.getTime());
                        seekBar.setProgress((int) mediaPlayer.getTime());
                        timerTextView.setText(getDuration((int) mediaPlayer.getTime()));
                        SettingsApp.getInstance().setCurrentPositions(mediaPlayer.getPosition());
                        SettingsApp.getInstance().setCurrentTime(mediaPlayer.getTime());
                        break;
                    case SeekableChanged:
                        Log.d("Loging_plaer", "SeekableChanged" + mediaPlayer.getTime());
                        break;
                    case PausableChanged:
                        Log.d("Loging_plaer", "PausableChanged" + mediaPlayer.getTime());
                        break;
                    case Vout:
                        Log.d("Loging_plaer", "Vout 274" + mediaPlayer.getTime());
                        break;
                    case ESAdded:
                        Log.d("Loging_plaer", "ESAdded 276" + mediaPlayer.getTime());
                        break;
                    case ESDeleted:
                        Log.d("Loging_plaer", "ESDeleted 277" + mediaPlayer.getTime());
                        break;
                }
            }
        });
        Media media = new Media(lib, Uri.fromFile(new File(videoFilePath)));
        media.setHWDecoderEnabled(true, false);
        mediaPlayer.setMedia(media);
        final IVLCVout vout = mediaPlayer.getVLCVout();
        vout.setVideoView(videoView);
        vout.attachViews();
        // setSize(100,100);
        mediaPlayer.play();
        if (SettingsApp.getInstance().getCurrentTime() > 0) {
//            Toast.makeText(MainActivity.this, " 262 __ " + SettingsApp.getInstance().getCurrentTime(), Toast.LENGTH_SHORT).show();
            mediaPlayer.setTime(SettingsApp.getInstance().getCurrentTime());
        }
        media.release();
        if (isSeek) {
            mediaPlayer.setPosition(SettingsApp.getInstance().getCurrentPositions());
            isSeek = false;
        }
        doHideControl(false);
    }

    private void startConnectDisk() {
        handlerConnectDisk.removeCallbacks(handlerConnectDiskProcessing);
        handlerConnectDisk.postDelayed(handlerConnectDiskProcessing, 1000);
    }

    private void hideControl() {
        if (hidenControl) {
            playButton.setVisibility(View.INVISIBLE);
            nextButton.setVisibility(View.INVISIBLE);
            prevButton.setVisibility(View.INVISIBLE);
            seekBar.setVisibility(View.INVISIBLE);
            timerTextView.setVisibility(View.INVISIBLE);
            timeDurationTextView.setVisibility(View.INVISIBLE);
            imageView.setVisibility(View.INVISIBLE);
        } else {
            playButton.setVisibility(View.VISIBLE);
            nextButton.setVisibility(View.VISIBLE);
            prevButton.setVisibility(View.VISIBLE);
            seekBar.setVisibility(View.VISIBLE);
            timerTextView.setVisibility(View.VISIBLE);
            timeDurationTextView.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.VISIBLE);


            handlerControl.removeCallbacks(runControl);
            handlerControl.postDelayed(runControl, 15000);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mediaPlayer != null) {
            mediaPlayer.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideSystemUI();
        doHideControl(true);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            if (videoFilePath != null && !videoFilePath.isEmpty()) {
                goToVideo();
            } else {
                Intent intent = getIntent();
                Uri data = intent.getData();
                if (data != null) {
                    m3uFilePath = data.getPath();
                    thread = new Thread(null, doBackgroundThreadProcessing3,
                            "Background");
                    thread.start();
                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            thread = new Thread(null, doBackgroundThreadProcessing,
                                    "Background");
                            thread.start();
                        }
                    }, 12000);
                }
            }
        } else {
            requestPermission();
        }
        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Activity.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock lock = keyguardManager.newKeyguardLock(KEYGUARD_SERVICE);
        lock.disableKeyguard();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        try {
            unregisterReceiver(usb_broadcastReceiver);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("WAKE_UP", "Error usb_broadcastReceiver");
        }
    }

    @OnClick({R.id.videoView,
            R.id.playButton,
            R.id.nextButton,
            R.id.prevButton})
    public void onKeyUpOnTouch(View view) {
        doHideControl(false);
        switch (view.getId()) {
            case R.id.playButton:
                if (mediaPlayer.isPlaying()) {
                    playButton.setImageResource(R.drawable.v_play_button);
                    mediaPlayer.pause();
                } else {
                    playButton.setImageResource(R.drawable.v_pause_button);
                    mediaPlayer.play();
                    doHideControl(true);
                }
                break;
            case R.id.nextButton:
                indexVideo++;
                prepereIndex(indexVideo);
                playButton.setImageResource(R.drawable.v_pause_button);
                break;
            case R.id.prevButton:
                indexVideo--;
                prepereIndex(indexVideo);
                playButton.setImageResource(R.drawable.v_pause_button);
                break;
        }
    }

    private void doHideControl(boolean b) {
        hidenControl = b;
        hideControl();
    }

    private String getDuration(long duration) {
        String minString;
        String secString;
        String hourString;
        long hour = TimeUnit.MILLISECONDS.toHours(duration);
        long min = 0;
        if (hour > 0) {
            min = TimeUnit.MILLISECONDS.toMinutes(duration) - 60;
        } else {
            min = TimeUnit.MILLISECONDS.toMinutes(duration);
            if (min < 10) {

            }
        }
        long sec = TimeUnit.MILLISECONDS.toSeconds(duration) -
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration));
        if (min < 10) {
            minString = String.format("0%d", min);
        } else {
            minString = String.format("%d", min);
        }
        if (sec < 10) {
            secString = String.format("0%d", sec);
        } else {
            secString = String.format("%d", sec);
        }
        if (hour < 10) {
            hourString = String.format("0%d", hour);
        } else {
            hourString = String.format("%d", hour);
        }
        return String.format("%s:%s:%s",
                hourString, minString, secString);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setSize(mVideoWidth, mVideoHeight);
    }

    private void setSize(int width, int height) {
        mVideoWidth = width;
        mVideoHeight = height;
        if (mVideoWidth * mVideoHeight <= 1)
            return;
        if (videoView == null || videoView == null)
            return;
// get screen size
        int w = getWindow().getDecorView().getWidth();
        int h = getWindow().getDecorView().getHeight();
// getWindow().getDecorView() doesn't always take orientation into
// account, we have to correct the values
        boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
        if (w > h && isPortrait || w < h && !isPortrait) {
            int i = w;
            w = h;
            h = i;
        }
        float videoAR = (float) mVideoWidth / (float) mVideoHeight;
        float screenAR = (float) w / (float) h;
        if (screenAR < videoAR)
            h = (int) (w / videoAR);
        else
            w = (int) (h * videoAR);
// force surface buffer size
        videoView.getHolder().setFixedSize(mVideoWidth, mVideoHeight);
// set display size
        ViewGroup.LayoutParams lp = videoView.getLayoutParams();
        lp.width = w;
        lp.height = h;
        videoView.setLayoutParams(lp);
        videoView.invalidate();
    }

    public class USB_BroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("USB_BroadcastReceiver", "Intent intent power");
            String action = intent.getAction();
            Log.d("USB_BroadcastReceiver", "Intent intent action  " + action);
            System.out.println("BroadcastReceiver Event");


        }
    }
}