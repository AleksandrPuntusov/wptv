package com.example.iwhitepixel.wptv;

import android.app.Application;
import android.content.Context;


public class App extends Application {

    public static App instance;
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this.getApplicationContext();
        instance = this;
    }

    public static Context getContext() {
        return context;
    }
}
